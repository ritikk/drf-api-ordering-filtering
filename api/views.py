from django.shortcuts import render
from .serializers import StudentSerializers
from rest_framework.generics import ListAPIView
from .models import Student
from rest_framework.filters import OrderingFilter


# Create your views here.
class StudentList(ListAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializers
    filter_backends = [OrderingFilter]
    ordering_fields = [
        'name']  # If we dont write this then it will show ordering for all the feilds in browsable api filter tab
